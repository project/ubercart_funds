<?php

namespace Drupal\ubercart_funds;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for the withdrawal method plugins.
 */
interface WithdrawalMethodInterface extends PluginInspectionInterface {

}
